name := "spark_mlib_project"

version := "1.0"

scalaVersion := "2.10.4"



libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.6.1",
  "org.apache.spark" % "spark-mllib_2.10" % "1.6.1",
  "org.apache.spark" %% "spark-sql" % "1.6.1",
  "com.typesafe" % "config" % "1.2.1",
  "commons-io" % "commons-io" % "2.4",
  "com.databricks" %% "spark-csv" % "1.0.3",
  "log4j" % "log4j" % "1.2.15",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test")