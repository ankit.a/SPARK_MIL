Prerequisites:
1.SBT
2.Spark-1.6.1
3.Scala-2.10.4

Changes:
1.Migrate to src/main/scala
2.Open classification.scala file.
3.Change input and output file path


Inputs:
1. CSV file of data.
(Please see resource folder)
/*****
 * Run this command in terminal from folder
******/
sh run.sh


Output:
1.In console it will show following things:
  *Tree Details
  *Tree Depth
  *Rules
  *Confusion Matrix
  *Mean Squared Error
  *Accuracy

2.A file will be generated in output path that contains feature value,actual value and predicted value.
