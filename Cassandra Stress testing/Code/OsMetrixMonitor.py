import psutil
import pprint
import os
import time
import sys


def main():
	data = ','.join(['Time','MemUsed(Percent)','MemCached','CpuLoad(Percent)','CpuUser(ms)','CpuPriviledge','CpuIdle','DiskUsage(Percent)','ReadThroughput(bytesPerSec)','WriteThroughput','DiskThroughput','ReadRate(OpsPerSec)','WriteRate','DiskLatency(TotalSecPerOPS)','DiskUtilization(ms)'])
	# with open(argv[0],'a') as f:
	# 	f.write(data)
	# 	f.write('\n')
	print data

	cpuData = psutil.cpu_times()
	CpuUserPrev = cpuData[0]
	CpuPriviledgePrev = cpuData[2]
	CpuIdlePrev = cpuData[3]

	diskdata = psutil.disk_io_counters()
	readBytesPrev = diskdata[2]
	writeBytesPrev = diskdata[3]
	readOpsPrev = diskdata[0]
	writeOpsPrev = diskdata[1]
	readTimePrev = diskdata[4]
	writeTimePrev = diskdata[5]
	diskUtilPrev = diskdata[8]

	t_start = time.time()
	t_end = t_start + 60 * 30

	while time.time() < t_end:
		Time = str(time.time() - t_start) 

		CpuLoad = str(psutil.cpu_percent(interval=None))

		cpuData = psutil.cpu_times()
		CpuUser = str(float(cpuData[0] - CpuUserPrev))
		CpuPriviledge = str(float(cpuData[2] - CpuPriviledgePrev))
		CpuIdle = str(float(cpuData[3] - CpuIdlePrev))

		CpuUserPrev = cpuData[0]
		CpuPriviledgePrev = cpuData[2]
		CpuIdlePrev = cpuData[3]

		memData = psutil.virtual_memory()
		MemUsed = str(memData[2])
		MemCached = str(float(memData[8]*100/memData[0]))

		DiskUsage = str(psutil.disk_usage('/')[3])

		diskdata = psutil.disk_io_counters()

		ReadThroughput = str(float(diskdata[2]-readBytesPrev))
		WriteThroughput = str(float(diskdata[3]-writeBytesPrev))
		DiskThroughput = str(float(diskdata[2]+diskdata[3])-float(readBytesPrev+writeBytesPrev))

		readBytesPrev = diskdata[2]
		writeBytesPrev = diskdata[3]

		ReadRate = str(float(diskdata[0]-readOpsPrev))
		WriteRate = str(float(diskdata[1]-writeOpsPrev))
		readOpsPrev = diskdata[0]
		writeOpsPrev = diskdata[1]

		DiskLatency = str(float(diskdata[4]+diskdata[5])-float(readTimePrev+writeTimePrev))
		readTimePrev = diskdata[4]
		writeTimePrev = diskdata[5]

		DiskUtilization = str(diskdata[8] - diskUtilPrev)
		diskUtilPrev = diskdata[8]


		data = ','.join([Time,MemUsed,MemCached,CpuLoad,CpuUser,CpuPriviledge,CpuIdle,DiskUsage,ReadThroughput,WriteThroughput,DiskThroughput,ReadRate,WriteRate,DiskLatency,DiskUtilization])
		# with open(argv[0],'a') as f:
		# 	f.write(data)
		# 	f.write('\n')
		print data

		time.sleep(1)



if __name__ == "__main__":
	main()
