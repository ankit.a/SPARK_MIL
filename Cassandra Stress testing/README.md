This project deals with stress testing Cassandra Database for the three compaction strategies that are currently available, namely :
1. STCS (Size Tiered Compaction Strategy)
2. LTCS (Leveled Tiered Compaction Strategy)
3. DTCS (Date Tiered Compaction Strategy)
